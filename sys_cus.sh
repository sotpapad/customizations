#!/bin/sh

### Populate HOME with useful directories.
mkdir -p ~/Codes/bin/ ~/Codes/python_work/ ~/git_stuff/ ~/.vim/colors/


### Changes to ~/.bashrc 

# Change to enable vimode.
ENABLE_VIMODE="set -o vi"

echo $ENABLE_VIMODE >> ~/.bashrc

# Set vim as default editor.
DEFEDITOR="export EDITOR='vim'"
DVISUAL="export VISUAL='vim'"

echo $DEFEDITOR >> ~/.bashrc
echo $DVISUAL >> ~/.bashrc


### Create python virtual environments.
# General environment.
python3 -m ~/Codes/.env/
# Psychopy-compatible environment.
python3 -m ~/Codes/.psyenv/

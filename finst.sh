#!/bin/bash/

### Installation of various packages in new system.

# Update repositories.
sudo apt update

# Get system architecture.
ARCH=$(arch) 
if [[$ARCH == "x86_64"]]; then
	FF="firefox"
elif [[$ARCH == "aarch64"]]; then
	FF="firefox-esr"
fi

# Assuming use of a debian-based system, install the following via 'apt'.
sudo apt install $FF libreoffice flatpak neofetch vim-nox lolcat \
kdeconnect pdfsam \
/
if [[$ARCH == "aarch64"]]; then
	sudo apt install thunderbird openvpn
fi

# Add flathub as repo for flatpak apps.
flatpak remote-add --if-not-exists https://flathub.org/repo/flathub.flatpakrepo

# Install flatpak apps.
flatpak install io.freetubeapp.FreeTube \
io.gitlab.GoodVibes \
org.gambus.gfeeds \
org.kde.occular \
net.jami.Jami \
/

if [[$ARCH == "x86_64"]]; then
	flatpak install org.inkscape.Inkscape \
	org.mozilla.Thunderbird
	com.nextcloud.desktopclient.nexcloud \
	im.riot.Riot \
	org.jitsi.jitsi-meet \
	org.deluge_torrent.deluge \
	com.valvesoftware.Steam \
	org.gnome.Cheese \
	org.videolan.VLC \
	/
fi
